"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.MaterialModule = void 0;
var core_1 = require("@angular/core");
var input_1 = require("@angular/material/input");
var form_field_1 = require("@angular/material/form-field");
var button_1 = require("@angular/material/button");
var stepper_1 = require("@angular/material/stepper");
var toolbar_1 = require("@angular/material/toolbar");
var badge_1 = require("@angular/material/badge");
var icon_1 = require("@angular/material/icon");
var select_1 = require("@angular/material/select");
var datepicker_1 = require("@angular/material/datepicker");
var material_moment_adapter_1 = require("@angular/material-moment-adapter");
var MaterialModule = /** @class */ (function () {
    function MaterialModule() {
    }
    MaterialModule = __decorate([
        core_1.NgModule({
            imports: [
                input_1.MatInputModule,
                form_field_1.MatFormFieldModule,
                button_1.MatButtonModule,
                stepper_1.MatStepperModule,
                toolbar_1.MatToolbarModule,
                badge_1.MatBadgeModule,
                icon_1.MatIconModule,
                select_1.MatSelectModule,
                datepicker_1.MatDatepickerModule,
                material_moment_adapter_1.MatMomentDateModule
            ],
            exports: [
                input_1.MatInputModule,
                form_field_1.MatFormFieldModule,
                button_1.MatButtonModule,
                stepper_1.MatStepperModule,
                toolbar_1.MatToolbarModule,
                icon_1.MatIconModule,
                select_1.MatSelectModule,
                badge_1.MatBadgeModule,
                material_moment_adapter_1.MatMomentDateModule,
                datepicker_1.MatDatepickerModule
            ]
        })
    ], MaterialModule);
    return MaterialModule;
}());
exports.MaterialModule = MaterialModule;
