import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { Authority } from './auth/auth-shared/constants/authority.constants';
import { UserRouteAccesService } from './auth/guards/user-route-acces.service';
import { AccessDeniedComponent } from './auth/access-denied/access-denied.component';
import { ForgotPasswordComponent } from './auth/forgot-password/forgot-password.component';
import { UsersCreateComponent } from './components/users/users-create/users-create.component';
import { DetailUserCreateComponent } from './components/detail-user/detail-user-create/detail-user-create.component';


const routes: Routes = [
  {
    path: 'dashboard',

    data: {
      authorities: [Authority.ADMIN, Authority.APRENDIZ, Authority.INSTRUCTOR, Authority.ASPIRANTE]
    },
    canActivate: [UserRouteAccesService],

    loadChildren: () => import('./dashboard/dashboard.module')
      .then(m => m.DashboardModule)
  },

  {
    path: 'login',
    component: LoginComponent
  },

  {
    path: 'access-denied',
    component: AccessDeniedComponent
  },
  {
    path: 'forgot-password',
    component: ForgotPasswordComponent
  },
  {
    path: 'userCreate',
    component: UsersCreateComponent
  },
  {
    path: 'detailUser-create/:id',
    component: DetailUserCreateComponent
  },

  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
