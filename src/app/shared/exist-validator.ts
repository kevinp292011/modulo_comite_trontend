import {FormControl} from '@angular/forms'
import { timer } from 'rxjs'
import { switchMap, map} from 'rxjs/operators'



export const existsAsyncValidator = (validatorService: any) => {
    return(input: FormControl) => {
        return timer(200).pipe(
            switchMap(() => validatorService.findUserByLogin({login: input.value}).pipe(
                map((res: any) => {
                    if (res && res.length > 0 ) {
                        return { exist: true};
                    }
                    return null;
                })
            ))
        );
    };
};

export const existsAsyncValidator1 = (ValidatorService: any) => {
    return(input: FormControl) => {
        return timer(200).pipe(
            switchMap(() => ValidatorService.findUserByEmail({email: input.value}).pipe(
                map((res: any) => {
                    if (res && res.length > 0 ) {
                        return { exist: true};
                    }
                    return null;
                })
            ))
        );
    };
};