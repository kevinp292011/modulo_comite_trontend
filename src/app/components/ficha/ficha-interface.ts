import { IProgram } from '../program/program-interface';

export interface IFicha {
    id:number,
    code:string,
    startDate:Date,
    status:boolean,
    dateEndLective:string,
    dateEndPractice:string,
    offerType:string,
    trainingType:string,
    workingDay:string,
    programs?:IProgram[]
}