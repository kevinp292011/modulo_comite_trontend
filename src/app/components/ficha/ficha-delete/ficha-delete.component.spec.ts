import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FichaDeleteComponent } from './ficha-delete.component';

describe('FichaDeleteComponent', () => {
  let component: FichaDeleteComponent;
  let fixture: ComponentFixture<FichaDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FichaDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FichaDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
