import { Component, OnInit } from '@angular/core';
import { IFicha } from '../ficha-interface';
import { FichaService } from '../ficha.service';
import { MenuItem } from 'primeng/api';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-ficha-list',
  templateUrl: './ficha-list.component.html',
  styleUrls: ['./ficha-list.component.styl']
})
export class FichaListComponent implements OnInit {
  breadcrumbItem: MenuItem[] = [];

  fichaList: IFicha[];
  ficha:IFicha;

  sizePage = 10;
  pageNumber = 0;
  listPages = [];
  constructor(private fichaService: FichaService,  private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.data.subscribe(data => {
      this.breadcrumbItem.push(data.breadcrumb);
    });
    this.initPagination(this.pageNumber);
  }
  initPagination(page: number): void {
    this.fichaService.query({
      'pageSize': this.sizePage,
      'pageNumber': page
    })
    .subscribe((res: any) => {
      this.fichaList = res.content;
      this.formatPagination(res.totalPages);

    }, err => console.error(err));
  }

  private formatPagination(totalPages: number): void {
    this.listPages = [];
    for(let i = 0; i < totalPages; i++){
      this.listPages.push(i);
    }
  }
}
