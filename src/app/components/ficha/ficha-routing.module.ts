import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FichaCreateComponent } from './ficha-create/ficha-create.component';
import { FichaListComponent } from './ficha-list/ficha-list.component';


const routes: Routes = [
  {
    path:"ficha-create",
    component:FichaCreateComponent

  },
  {
    path:"ficha-list",
    data: {
      breadcrumb: {
        label: "Fichas",
        url: "ficha"
      }
    },
    component:FichaListComponent

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FichaRoutingModule { }
