import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { FichaService } from '../ficha.service';
import { IProgram } from '../../program/program-interface';
import { ProgramService } from '../../program/program.service';

@Component({
  selector: 'app-ficha-create',
  templateUrl: './ficha-create.component.html',
  styleUrls: ['./ficha-create.component.styl']
})
export class FichaCreateComponent implements OnInit {
  programList: IProgram[];
  program: IProgram;
  selectedProgramTemp: IProgram;
  sizePage = 10;
  pageNumber = 0;
  listPages = [];

  /**Metod err message for imputs form */
  date = new FormControl('', [Validators.required]);
  dateOne = new FormControl('', [Validators.required]);
  dateTwo = new FormControl('', [Validators.required]);
  dateThree = new FormControl('', [Validators.required]);

  getErrorMessageDate() {
    if (this.date.hasError('required')) {
      return 'Ingrese una fecha';
    }
  }

  getErrorMessageDateOne() {
    if (this.dateOne.hasError('required')) {
      return 'Ingrese una fecha';
    }
  }

  getErrorMessageDateTwo() {
    if (this.dateTwo.hasError('required')) {
      return 'Ingrese una fecha';
    }
  }

  getErrorMessageDateThree() {
    if (this.dateThree.hasError('required')) {
      return 'Ingrese una fecha';
    }
  }


  /**End erros message */


  fichaFormGroup: FormGroup;
  constructor(private formBuilder: FormBuilder, private fichaService: FichaService, private programService: ProgramService) {
    this.fichaFormGroup = this.formBuilder.group({
      code: ['', [Validators.required,
      Validators.maxLength(10)
      ]],
      status: [''],
      offerType: ['', [Validators.required
      ]],
      trainingType: ['', [Validators.required
      ]],
      workingDay: ['', [Validators.required
      ]],
      program: this.selectedProgramTemp
    });
  }

  ngOnInit() {
    this.initPagination(this.pageNumber);
  }

  initPagination(page: number): void {
    this.programService.query({
      'pageSize': this.sizePage,
      'pageNumber': page
    })
      .subscribe((res: any) => {
        this.programList = res.content;
        this.formatPagination(res.totalPages);

      }, err => console.error(err));
  }
  private formatPagination(totalPages: number): void {
    this.listPages = [];
    for (let i = 0; i < totalPages; i++) {
      this.listPages.push(i);
    }
  }




  public saveFicha(): void {

    this.fichaService.saveFicha(this.fichaFormGroup.value)
      .subscribe(res => {
      }, error => {
      });
  }

  addToProgram(program: IProgram): void {
    this.selectedProgramTemp = program;
    this.fichaFormGroup = this.formBuilder.group({
      code: ['', [Validators.required,
      Validators.maxLength(10)
      ]],
      startDate: ['', [
        Validators.maxLength(11)
      ]],
      status: [''],
      dateEndLective: ['', [
        Validators.maxLength(11)
      ]],
      dateEndPractice: ['', [
        Validators.maxLength(11)
      ]],
      offerType: ['', [Validators.required
      ]],
      trainingType: ['', [Validators.required
      ]],
      workingDay: ['', [Validators.required
      ]],
      program: this.selectedProgramTemp
    });
  }
}
