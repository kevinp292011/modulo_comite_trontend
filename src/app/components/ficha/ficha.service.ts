import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { createRequestOption } from '../../utils/request_utils';
import { Observable, pipe } from 'rxjs';

import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { IFicha } from './ficha-interface';



@Injectable({
  providedIn: 'root'
})
export class FichaService {

  constructor(private http: HttpClient) { }

  public query(req?: any): Observable<IFicha[]> {
    let params = createRequestOption(req);
    return this.http.get<IFicha[]>(`${environment.END_POINT}/api/ficha`, { params: params })
      .pipe(map(res => {
        return res;
      }));
  }

  public saveFicha(ficha: IFicha[]): Observable <IFicha> {
  return this.http.post<IFicha>(`${environment.END_POINT}/api/ficha`, ficha)
    .pipe(map(res => {
      return res;
    }));
}
public searchFichaByCode(req?: any): Observable<IFicha[]> {
  let params = createRequestOption(req);

  return this.http.get<IFicha[]>(`${environment.END_POINT}/api/ficha/search`,{params: params})
  .pipe(map(res => {
    return res;
  }));
}

}



