import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FichaRoutingModule } from './ficha-routing.module';
import { FichaCreateComponent } from './ficha-create/ficha-create.component';
import { FichaListComponent } from './ficha-list/ficha-list.component';
import { FichaUpdateComponent } from './ficha-update/ficha-update.component';
import { FichaDeleteComponent } from './ficha-delete/ficha-delete.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../material.module'
import {BreadcrumbModule} from 'primeng/breadcrumb';


@NgModule({
  declarations: [FichaCreateComponent, FichaListComponent, FichaUpdateComponent, FichaDeleteComponent],
  imports: [
    CommonModule,
    FichaRoutingModule,
    ReactiveFormsModule,
    MaterialModule,
    BreadcrumbModule
  ]
})
export class FichaModule { }
