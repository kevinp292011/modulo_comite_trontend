import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserHasFichaUpdateComponent } from './user-has-ficha-update.component';

describe('UserHasFichaUpdateComponent', () => {
  let component: UserHasFichaUpdateComponent;
  let fixture: ComponentFixture<UserHasFichaUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserHasFichaUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserHasFichaUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
