import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersHasFichaRoutingModule } from './users-has-ficha-routing.module';
import { UserHasFichaCreateComponent } from './user-has-ficha-create/user-has-ficha-create.component';
import { UserHasFichaListComponent } from './user-has-ficha-list/user-has-ficha-list.component';
import { UserHasFichaUpdateComponent } from './user-has-ficha-update/user-has-ficha-update.component';
import { UserHasFichaDeleteComponent } from './user-has-ficha-delete/user-has-ficha-delete.component';


@NgModule({
  declarations: [UserHasFichaCreateComponent, UserHasFichaListComponent, UserHasFichaUpdateComponent, UserHasFichaDeleteComponent],
  imports: [
    CommonModule,
    UsersHasFichaRoutingModule
  ]
})
export class UsersHasFichaModule { }
