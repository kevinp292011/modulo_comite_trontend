import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserHasFichaDeleteComponent } from './user-has-ficha-delete.component';

describe('UserHasFichaDeleteComponent', () => {
  let component: UserHasFichaDeleteComponent;
  let fixture: ComponentFixture<UserHasFichaDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserHasFichaDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserHasFichaDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
