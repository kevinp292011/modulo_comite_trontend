import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserHasFichaCreateComponent } from './user-has-ficha-create.component';

describe('UserHasFichaCreateComponent', () => {
  let component: UserHasFichaCreateComponent;
  let fixture: ComponentFixture<UserHasFichaCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserHasFichaCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserHasFichaCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
