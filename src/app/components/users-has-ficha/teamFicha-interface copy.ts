import { IDetailUser } from '../detail-user/detail-user-interface';
import { IFicha } from '../ficha/ficha-interface';


export interface ITeamFicha2 {
    id?:number,
    rolUserFicha:string,
    detailUser: IDetailUser,
    ficha:IFicha 
}