

export interface ITeamFicha {
    id?:number,
    documentType:string,
    fullName: string,
    emailPersonal:string,
    documentNumber: string,
    codeFicha: string,
    phone: number
    
}