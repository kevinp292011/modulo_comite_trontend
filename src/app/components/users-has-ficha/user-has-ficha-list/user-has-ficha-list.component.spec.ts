import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserHasFichaListComponent } from './user-has-ficha-list.component';

describe('UserHasFichaListComponent', () => {
  let component: UserHasFichaListComponent;
  let fixture: ComponentFixture<UserHasFichaListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserHasFichaListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserHasFichaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
