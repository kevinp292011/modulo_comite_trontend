import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProgramRoutingModule } from './program-routing.module';
import { ProgramCreateComponent } from './program-create/program-create.component';
import { ProgramListComponent } from './program-list/program-list.component';
import { ProgramUpdateComponent } from './program-update/program-update.component';
import { ProgramDeleteComponent } from './program-delete/program-delete.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../material.module';


@NgModule({
  declarations: [ProgramCreateComponent, ProgramListComponent, ProgramUpdateComponent, ProgramDeleteComponent],
  imports: [
    CommonModule,
    ProgramRoutingModule,
    ReactiveFormsModule,
    MaterialModule
  ]
})
export class ProgramModule { }
