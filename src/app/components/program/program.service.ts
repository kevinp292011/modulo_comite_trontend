import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { createRequestOption } from '../../utils/request_utils';
import { Observable} from 'rxjs';

import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { IProgram } from './program-interface';



@Injectable({
  providedIn: 'root'
})
export class ProgramService {

  constructor(private http: HttpClient) { }

  public query(req?: any): Observable<IProgram[]> {
    let params = createRequestOption(req);
    return this.http.get<IProgram[]>(`${environment.END_POINT}/api/programs`, { params: params })
      .pipe(map(res => {
        return res;
      }));
  }

  public saveProgram(program: IProgram): Observable <IProgram> {
  return this.http.post<IProgram>(`${environment.END_POINT}/api/programs`, program)
    .pipe(map(res => {
      return res;
    }));
}

}



