import { Component, OnInit } from '@angular/core';
import { IProgram } from '../program-interface';
import { ProgramService } from '../program.service';


@Component({
  selector: 'app-program-list',
  templateUrl: './program-list.component.html',
  styleUrls: ['./program-list.component.styl']
})
export class ProgramListComponent implements OnInit {

  programList: IProgram[];
  program:IProgram;

  sizePage = 10;
  pageNumber = 0;
  listPages = [];
  constructor(private programService: ProgramService) { }

  ngOnInit() {
    this.initPagination(this.pageNumber);
  }
  initPagination(page: number): void {
    this.programService.query({
      'pageSize': this.sizePage,
      'pageNumber': page
    })
    .subscribe((res: any) => {
      this.programList = res.content;
      

      this.formatPagination(res.totalPages);

    }, err => console.error(err));
  }

  private formatPagination(totalPages: number): void {
    this.listPages = [];
    for(let i = 0; i < totalPages; i++){
      this.listPages.push(i);
    }
  }

}
