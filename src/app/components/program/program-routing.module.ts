import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProgramCreateComponent } from './program-create/program-create.component';
import { ProgramListComponent } from './program-list/program-list.component';


const routes: Routes = [
  {
    path:"program-create",
    component:ProgramCreateComponent
  },
  {
    path:"program-list",
    component:ProgramListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProgramRoutingModule { }
