

export interface IProgram {
    id:number,
    code:String,
    version:String,
    nameProgram:String,
    durationTrimesterProductive:number,
    status: boolean,
    justification: String,
    durationTrimesterLective: number,
    trainingMode:String,
    trainingType:String
}