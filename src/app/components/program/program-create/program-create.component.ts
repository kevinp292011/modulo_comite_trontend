import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProgramService } from '../program.service';

@Component({
  selector: 'app-program-create',
  templateUrl: './program-create.component.html',
  styleUrls: ['./program-create.component.styl']
})
export class ProgramCreateComponent implements OnInit {
  selectedProgram: number;

  programFormGroup: FormGroup;
  constructor(private formBuilder: FormBuilder, private programService: ProgramService) {
    this.programFormGroup = this.formBuilder.group({
      code: ['', [
        Validators.maxLength(6)
        ]],
      nameProgram: ['', [
      Validators.maxLength(100)
      ]],
      version: ['', [Validators.required,
      Validators.maxLength(4)
      ]],
      durationTrimesterProductive: ['', [Validators.required,
      Validators.maxLength(50)
      ]],
      durationTrimesterLective:['', [Validators.required,
        Validators.maxLength(50)
        ]],
        trainingType: ['', [Validators.required
        ]],
        justification: ['', [Validators.required
        ]],
      trainingMode: ['', [Validators.required
      ]],
      status: true
    });
  }

  ngOnInit() {
  }
  public saveProgram(): void {
    this.programService.saveProgram(this.programFormGroup.value)
      .subscribe(res => {

      }, error => {
      });
  }

}
