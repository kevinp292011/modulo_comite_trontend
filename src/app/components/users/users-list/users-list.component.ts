import { Component, OnInit } from '@angular/core';
import { IUsers } from '../users-interface';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.styl']
})
export class UsersListComponent implements OnInit {
  usersList: IUsers[];
  users:IUsers;

  sizePage = 10;
  pageNumber = 0;
  listPages = [];
  
  constructor(private UsersService: UsersService) { }

  ngOnInit() {
    this.initPagination(this.pageNumber);
  }
  initPagination(page: number): void {
    this.UsersService.query({
      'pageSize': this.sizePage,
      'pageNumber': page
    })
    .subscribe((res: any) => {
      this.usersList = res.content;
      this.formatPagination(res.totalPages);

    }, err => console.error(err));
  }

  private formatPagination(totalPages: number): void {
    this.listPages = [];
    for(let i = 0; i < totalPages; i++){
      this.listPages.push(i);
    }
  }
}
