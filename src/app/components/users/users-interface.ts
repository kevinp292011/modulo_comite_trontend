
export interface IUsers {
    id:number,
    login:string,
    password:string,
    firstName:string,
    lastName:string,
    email:string,
    activated:boolean,
    createdBy:string,
    lastModifiedBy:string
}
