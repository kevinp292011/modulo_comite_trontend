import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { IUsers } from './users-interface';
import { createRequestOption } from '../../utils/request_utils';
import { Observable, pipe } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { }

  public query(req?: any): Observable<IUsers[]> {
    let params = createRequestOption(req);
    return this.http.get<IUsers[]>(`${environment.END_POINT}/api/user`, { params: params })
      .pipe(map(res => {
        return res;
      }));
  }
  public saveUser(users: IUsers): Observable<IUsers> {
    return this.http.post<IUsers>(`${environment.END_POINT}/api/create`, users)
      .pipe(map(res => {
        return res;
      }));
  }
  public searchUsers(req?: any): Observable<IUsers[]> {
    let params = createRequestOption(req);

    return this.http.get<IUsers[]>(`${environment.END_POINT}/api/user/search`, { params: params })
      .pipe(map(res => {
        return res;
      }));
  }

  public getUsersById(id: string): Observable<IUsers> {
    return this.http.get<IUsers>(`${environment.END_POINT}/api/user/search/${id}`)
      .pipe(map(res => {
        return res;
      }));
  }
  /*public findUserByLogin(login: string): Observable <IUsers> {
    const params = createRequestOption(login);
    console.log("params", params);
    return this.http.get<IUsers>(`${environment.END_POINT}/api/user/validation-login`, { params: params })
    .pipe(map (res => {
      return res;
    }));
  }*/

  public findUserByLogin(req?: any): Observable<IUsers> {
    let params = createRequestOption(req);
  
    return this.http.get<IUsers>(`${environment.END_POINT}/api/user/validation`,{params: params})
    .pipe(map(res => {
      return res;
    }));
  }

  /*public findUserByEmail(req?: any): Observable<IUsers> {
    let params = createRequestOption(req);
  
    return this.http.get<IUsers>(`${environment.END_POINT}/api/user/email`,{params: params})
    .pipe(map(res => {
      return res;
    }));
  }*/

  public findUserByEmail(email: string): Observable <IUsers> {
    const params = createRequestOption(email);
    console.log("params", params);
    return this.http.get<IUsers>(`${environment.END_POINT}/api/user/email`, { params: params })
    .pipe(map (res => {
      return res;
    }));
  }

}

