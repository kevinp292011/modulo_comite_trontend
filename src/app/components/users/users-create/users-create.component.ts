import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { exists } from 'fs';
import { UsersService } from '../users.service';
import { existsAsyncValidator, existsAsyncValidator1 } from '../../../shared/exist-validator';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users-create',
  templateUrl: './users-create.component.html',
  styleUrls: ['./users-create.component.styl']
})
export class UsersCreateComponent implements OnInit {

  /**pasword */
  hide = true
  /**pasword */

  selectedApprentice: number;
  UserFormGroup: FormGroup;


  constructor(
    private route: Router,
    private formBuilder: FormBuilder, private usersService: UsersService) {

    this.UserFormGroup = this.formBuilder.group({
      login: ['', [Validators.required, Validators.minLength(3)], existsAsyncValidator(this.usersService)],
      password: ['', [
        Validators.required
      ]],
      firstName: ['', [
        Validators.required, Validators.minLength(3)
      ]],
      lastName: ['', [
        Validators.required, Validators.minLength(3)
      ]],
      status: true
    });
  }

  ngOnInit() {

  }

  public saveUser(): void {
    this.usersService.saveUser(this.UserFormGroup.value)
      .subscribe(res => {
        this.route.navigate(['../detailUser-create', res.id]);
      }, error => {
      });
  }
}