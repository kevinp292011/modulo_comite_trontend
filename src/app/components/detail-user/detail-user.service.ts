import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { createRequestOption } from '../../utils/request_utils';
import { Observable, pipe } from 'rxjs';

import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { IDetailUser } from './detail-user-interface';
import { ITeamFicha2 } from '../users-has-ficha/teamFicha-interface copy';



@Injectable({
  providedIn: 'root'
})
export class DetailUserService {
  saveForm(value: any) {
    throw new Error("Method not implemented.");
  }

  constructor(private http: HttpClient) { }

  public query(req?: any): Observable<IDetailUser[]> {
    let params = createRequestOption(req);
    return this.http.get<IDetailUser[]>(`${environment.END_POINT}/api/detailUser`, { params: params })
      .pipe(map(res => {
        return res;
      }));
  }

  public saveDetailUser(detailUser: IDetailUser): Observable <IDetailUser> {
  return this.http.post<IDetailUser>(`${environment.END_POINT}/api/detailUser`, detailUser)
    .pipe(map(res => {
      return res;
    }));
}
public getDetailUserByDocumentNumber(documentNumber: string): Observable<IDetailUser> {
  let params = new HttpParams();
  params = params.set('documentNumber',documentNumber);
  return this.http.get<IDetailUser>(`${environment.END_POINT}/api/detailUser/search`,{params: params})
  .pipe(map(res => {
    return res;
  }));
}

public searchUsers(req?: any): Observable<ITeamFicha2[]> {
  let params = createRequestOption(req);

  return this.http.get<ITeamFicha2[]>(`${environment.END_POINT}/api/teamFicha/search-Apprentice`,{params: params})
  .pipe(map(res => {
    return res;
  }));
}

public searchInstructor(req?: any): Observable<ITeamFicha2[]> {
  let params = createRequestOption(req);

  return this.http.get<ITeamFicha2[]>(`${environment.END_POINT}/api/teamFicha/search-Instructor`,{params: params})
  .pipe(map(res => {
    return res;
  }));
}

}



