import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetailUserListComponent } from './detail-user-list/detail-user-list.component';
import { DetailUserRoutingModule } from './detail-user-routing.module';
import { DetailUserCreateComponent } from './detail-user-create/detail-user-create.component';
import { ReactiveFormsModule } from '@angular/forms';
import {MaterialModule} from '../../material.module'



@NgModule({
  declarations: [DetailUserListComponent, DetailUserCreateComponent],
  imports: [
    CommonModule,
    DetailUserRoutingModule,
    ReactiveFormsModule,
    MaterialModule
  ]
})
export class DetailUserModule { }
