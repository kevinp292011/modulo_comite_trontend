import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailUserListComponent } from './detail-user-list/detail-user-list.component';
import { DetailUserCreateComponent } from './detail-user-create/detail-user-create.component';

const routes: Routes = [
  {
    path:"detailUser-create",
    component: DetailUserCreateComponent
  },
{
  path:"detailUser-list",
  component: DetailUserListComponent
}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DetailUserRoutingModule { }
