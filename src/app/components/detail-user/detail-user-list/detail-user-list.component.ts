import { Component, OnInit } from '@angular/core';
import { IDetailUser } from '../detail-user-interface';
import {  DetailUserService } from '../detail-user.service';

@Component({
  selector: 'app-detail-user-list',
  templateUrl: './detail-user-list.component.html',
  styleUrls: ['./detail-user-list.component.styl']
})
export class DetailUserListComponent implements OnInit {
  detailUserList: IDetailUser[];
  detailUser:IDetailUser;

  sizePage = 10;
  pageNumber = 0;
  listPages = [];
  constructor(private detailsUsersService: DetailUserService) { }

  ngOnInit() {
    this.initPagination(this.pageNumber);
  }
  initPagination(page: number): void {
    this.detailsUsersService.query({
      'pageSize': this.sizePage,
      'pageNumber': page
    })
    .subscribe((res: any) => {
      this.detailUserList = res.content;
      this.formatPagination(res.totalPages);
    }, err => console.error(err));
  }

  private formatPagination(totalPages: number): void {
    this.listPages = [];
    for(let i = 0; i < totalPages; i++){
      this.listPages.push(i);
    }
  }

}
