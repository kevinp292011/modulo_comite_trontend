import { IUsers } from '../users/users-interface';

export interface IDetailUser {
    id?:number,
    documentNumber?:string,
    phone?: string,
    emailPersonal?:string,
    address?:string,
    expeditionDate?:string,
    gender:string,
    documentType?:string,
    emailSena?:string,
    funcionario?:boolean,
    contractTerminated?:boolean,
    dateStartContract?:string,
    dateEndContract?:string,
    users?:IUsers
}