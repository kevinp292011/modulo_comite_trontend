import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailUserCreateComponent } from './detail-user-create.component';

describe('DetailUserCreateComponent', () => {
  let component: DetailUserCreateComponent;
  let fixture: ComponentFixture<DetailUserCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailUserCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailUserCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
