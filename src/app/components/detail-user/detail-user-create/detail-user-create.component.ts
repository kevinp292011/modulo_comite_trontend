import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { DetailUserService } from '../detail-user.service';
import { IUsers } from '../../users/users-interface';
import { UsersService } from '../../users/users.service';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { IDetailUser } from '../detail-user-interface';

@Component({
  selector: 'app-detail-user-create',
  templateUrl: './detail-user-create.component.html',
  styleUrls: ['./detail-user-create.component.styl']
})
export class DetailUserCreateComponent implements OnInit {

  usersList: IUsers[];
  users: IUsers;
  selectedUserTemp: IUsers;
  sizePage = 10;
  pageNumber = 0;
  listPages = [];
  detailUserFormGroup: FormGroup;

  /**Email Validations Material Form*/
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private detailUserService: DetailUserService,
    private UsersService: UsersService) {
    this.detailUserFormGroup = this.formBuilder.group({
      documentNumber: ['', [
        Validators.maxLength(15),
        Validators.minLength(8)
      ]],
      phone: ['', [
        Validators.required,
        Validators.maxLength(10),
        Validators.minLength(8)
      ]],
      emailPersonal: ['', [
        Validators.required,
        Validators.email,
        Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")
      ]],
      address: ['', [
        Validators.required,
        Validators.maxLength(60)
      ]],
      gender: ['', [
        Validators.required
      ]],
      documentType: ['', [
        Validators.required
      ]],

      emailSena: ['',
        [Validators.email,
        Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")
      ]]
    });

  }
  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    this.UsersService.getUsersById(id)
      .subscribe(res => {
        this.users = (res);
        return this.users;
      });
    //this.initPagination(this.pageNumber);
  }
 
  addToUser(user: IUsers): void {
    this.selectedUserTemp = user;
  }

  public saveDetailUser() {
    let saveDetailUser: IDetailUser = {
     
      documentNumber: this.detailUserFormGroup.get("documentNumber").value,
      phone: this.detailUserFormGroup.get("phone").value,
      emailPersonal: this.detailUserFormGroup.get("emailPersonal").value,
      address: this.detailUserFormGroup.get("address").value,
      expeditionDate: '2020-02-12',
      gender: this.detailUserFormGroup.get("gender").value,
      documentType: this.detailUserFormGroup.get("documentType").value,
      emailSena: this.detailUserFormGroup.get("emailSena").value,
      dateStartContract: '2020-02-12',
      dateEndContract: '2020-02-12',
      users: this.users[0]
    }


    this.detailUserService.saveDetailUser(saveDetailUser)
      .subscribe(res => {
        Swal.fire('¡Bienvenido!', 'Se ha registrado con exito', 'success');
        this.router.navigate(['../login']);

      }, error => {
        Swal.fire('¡OOPS!', 'No se pudo registrar correctamente', 'error');
      });
  }
}
