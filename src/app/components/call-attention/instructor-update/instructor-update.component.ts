import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ICallAttention } from '../call-attention-interface';
import { CallAttentionService } from '../call-attention.service';
import { ConfirmationService } from 'primeng/api';
import swal from 'sweetalert2';

@Component({
  selector: 'app-instructor-update',
  templateUrl: './instructor-update.component.html',
  styleUrls: ['./instructor-update.component.styl'],
  providers: [ConfirmationService]
})
export class InstructorUpdateComponent implements OnInit {
  attentionCall: ICallAttention;
  attentionCallFormUpdateGroup: FormGroup;


  constructor(private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private confirmationService: ConfirmationService,
    private router: Router,
    private callAttentionService: CallAttentionService) {
    this.attentionCallFormUpdateGroup = this.formBuilder.group({
      typeDocument: ['', [
        Validators.required
      ]],
      documentNumber: ['', [
        Validators.required
      ]],
      name: ['', [
        Validators.required
      ]],
      description: ['', [
        Validators.required
      ]],
      activities: ['', [
        Validators.required
      ]],
      type: ['', [
        Validators.required
      ]],
      trimestre: ['', [
        Validators.required
      ]]
    });
  }

  ngOnInit(): void {
    let id = this.activatedRoute.snapshot.paramMap.get('id');

    this.callAttentionService.getCallAttentionById({ 'id': id })
      .subscribe(res => {
        this.attentionCall = (res);
        this.loadForm(this.attentionCall);
      });
  }

  updateCallAttention() {
    let call: ICallAttention = {
      id: this.attentionCall.id,
      situation: this.attentionCallFormUpdateGroup.get("description").value,
      summary: this.attentionCallFormUpdateGroup.get("activities").value,
      commitment: '',
      ficha: this.attentionCall.ficha,
      aprendiz: this.attentionCall.aprendiz,
      instructor: this.attentionCall.instructor,
      status: this.attentionCall.status,
      typeCallAttention: this.attentionCallFormUpdateGroup.get("type").value,
      callAttentionDate: this.attentionCall.callAttentionDate,
      trimestre: this.attentionCallFormUpdateGroup.get("trimestre").value
    }
    swal.fire('Llamado de atención', 'Se esta actualizando el llamado de atención', 'warning');
    this.callAttentionService.updateCallAttention(call)
      .subscribe(res => {
        swal.fire('Llamado de atención', 'Se actualizo exitosamente el llamado de atención', 'success');
        this.router.navigate(['../dashboard/call-attention/instructor-call-attentionList']); // Redireccionar
      }, err => {
        swal.fire('¡OOPS!', 'No se pudo actualizar satisfactoriamente tu llamado de atencion', 'error');
      });
  }


  private loadForm(attentionCall: ICallAttention) {
    this.attentionCallFormUpdateGroup.patchValue({
      typeDocument: attentionCall.aprendiz.documentType,
      documentNumber: attentionCall.aprendiz.documentNumber,
      name: attentionCall.aprendiz.users.firstName,
      description: attentionCall.situation,
      activities: attentionCall.summary,
      type: attentionCall.typeCallAttention,
      trimestre: attentionCall.trimestre
    });
  }
  confirm() {
    this.confirmationService.confirm({
      message: '¿Está seguro que desea actualizar el formulario?',
      accept: () => {
        this.updateCallAttention()
      },
      reject: () => {
      }
    });
  }

}
