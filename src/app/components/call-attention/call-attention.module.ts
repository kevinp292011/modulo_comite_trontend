import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AprendizComponent } from './aprendiz/aprendiz.component';
import { InstructorComponent } from './instructor/instructor.component';
import { CallAttentionRoutingModule } from './call-attention-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/material.module';
import { InstructorListComponent } from './instructor-list/instructor-list.component';
import {InputSwitchModule} from 'primeng/inputswitch';
import { InstructorUpdateComponent } from './instructor-update/instructor-update.component';
import { AprendizListComponent } from './aprendiz-list/aprendiz-list.component';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {AutoCompleteModule} from 'primeng/autocomplete';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { AuthSharedModule } from 'src/app/auth/auth-shared/auth-shared.module';
import {DialogModule} from 'primeng/dialog';
import {DropdownModule} from 'primeng/dropdown'



@NgModule({
  declarations: [AprendizComponent, InstructorComponent, InstructorListComponent, InstructorUpdateComponent, AprendizListComponent],
  imports: [
    CommonModule,
    CallAttentionRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    InputSwitchModule,
    ConfirmDialogModule,
    AutoCompleteModule,
    MatInputModule,
    MatAutocompleteModule,
    AuthSharedModule,
    DialogModule,
    DropdownModule

  ]
})
export class CallAttentionModule { }
