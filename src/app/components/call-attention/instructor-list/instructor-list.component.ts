import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { CallAttentionService } from '../call-attention.service';
import { ICallAttention } from '../call-attention-interface';
import { UsersService } from '../../users/users.service';
import { FichaService } from '../../ficha/ficha.service';
import { IFicha } from '../../ficha/ficha-interface';
import { DetailUserService } from '../../detail-user/detail-user.service';
import { ITeamFicha2 } from '../../users-has-ficha/teamFicha-interface copy';
import { faEye } from '@fortawesome/free-solid-svg-icons';
import { MatDialog } from '@angular/material/dialog';
import { InstructorUpdateComponent } from '../instructor-update/instructor-update.component';
@Component({
  selector: 'app-instructor-list',
  templateUrl: './instructor-list.component.html',
  styleUrls: ['./instructor-list.component.styl']
})
export class InstructorListComponent implements OnInit {
  noMostrar= 1;

  idDataUser: number;
 
  
  ngOnInit() {
      this.Paginacion(0);
    
  }


  callAttentionList:ICallAttention[];
  fichaList:IFicha[] = []; 
  selectFichaTemp:IFicha;
  apprenticeList:ITeamFicha2[] = [];
  selectApprenticeTemp:ITeamFicha2;
  apprenticeListSF:ITeamFicha2[] = [];
  j:ITeamFicha2[] = [];
  instructorList:ITeamFicha2[] = [];
  selectInstructorTemp:ITeamFicha2;
  sizePage = 5;
  listPages = [];
  page = 0;
  contador = 0;
  faEye = faEye;
  selectCall: ICallAttention;

  searchForm = this.formBuilder.group({
    code:'',
    documentApprentice:'',
    documentInstructor: '',
    status:'',
    typeCallAttention:'',
    trimestre:''
});
filters = {
  code:'',
  documentApprentice:'',
  documentInstructor: '',
  status:'',
  typeCallAttention:'',
  trimestre:'',
  pageSize:this.sizePage,
  pageNumber: this.page
};

  constructor(
    public dialog: MatDialog,
    private formBuilder: FormBuilder, 
    private callAttentionService: CallAttentionService, 
    private userSrvice:UsersService,
    private fichaService:FichaService,
    private detailUserService:DetailUserService
    ) { }

  Paginacion(page1: number){
    this.page = null;
    this.page = (page1);
      if(this.contador != 0){
        this.searchApprenticeAndInstructorByFicha();
      }
  } 
//busqueda de autoComplic para ficha
  searchFichaByCode(event: any): void {
    this.fichaService.searchFichaByCode({
      'code': event.query
    })
    .subscribe(res =>{
      this.fichaList = res;
    })
  }
  //seleccion de un aprendiz 
  selectFichatemp(ficha: IFicha):void{
    this.selectFichaTemp = (ficha);
    this.searchApprenticeAndInstructorByFicha();
  }
  //seleccion de un aprendiz end
//busqueda de autoComplic para ficha end

//busqueda de autoComplic para aprendiz
  searchApprentice(event: any): void {
    this.j = [];
    this.detailUserService.searchUsers({
      'firstName': event.query
    })
    .subscribe(res =>{
      this.filterData(res);
      this.apprenticeList = (this.j);
    })
  }
  //seleccion de un aprendiz 
  selectAppreticeTemp(user: ITeamFicha2):void{
    this.selectApprenticeTemp = (user);
    this.searchApprenticeAndInstructorByFicha();
  }
  //seleccion de un aprendiz end
//busqueda de autoComplic para aprendiz end

//busqueda de autoComplic para aprendiz
searchInstructor(event: any): void {
  this.j = [];
  this.detailUserService.searchInstructor({
    'firstName': event.query
  })
  .subscribe(res =>{
    this.filterData(res);
    this.instructorList = (this.j);
  })
}
//seleccion de un aprendiz 
selectInstructortemp(user: ITeamFicha2):void{
  this.selectInstructorTemp = (user);
  this.searchApprenticeAndInstructorByFicha();
}
//seleccion de un aprendiz end
//busqueda de autoComplic para aprendiz end

  searchApprenticeAndInstructorByFicha(){
    this.contador++;
    this.callAttentionList = [];
    this.createFilterParams();
    this.callAttentionService.searchCallAttentionFilter(this.filters)
    .subscribe((res: any) =>{
      this.callAttentionList = res.content;
      this.formatPagination(res.totalPages);
    }, err => console.error(err))
    
  }
  //seleccionar llamado de atencion
  addCall(callA: ICallAttention){
    this.selectCall = callA;
  }
  //seleccionar llamado de atencion end

  changeStatuscallAttention(item: ICallAttention): void{
    this.callAttentionService.updateCallAttention(item)
    .subscribe(res =>{
    })
  }

  private filterData(datos:ITeamFicha2[] = []): void {
    let contador = 0;
    let validar = 0;
    for(var g = 0; g < datos.length; g++){
      
      for(var s = 1; s < datos.length; s++){
        if(g != s){
          if(datos[g].detailUser.documentNumber == datos[s].detailUser.documentNumber){
            validar++;
          }
        }
        
      }
    }
    if(validar != 0){
      for(let i = 0; i < datos.length; i++){
        contador = 0;
        for(let j = 1; j < datos.length; j++){
          while(datos[j] == undefined && j < datos.length || datos[i].id == datos[j].id && j < datos.length){
            j += 1;
            if(j >= datos.length){
              break;
            }
          }
          if(datos[j] != undefined){
            if(datos[i].detailUser.documentNumber == datos[j].detailUser.documentNumber){
              delete datos[j];
              contador++;
            } 
          }
        }
        if(contador != 0){
          i += contador;
        }
      }
    }
    for(let k = 0; k < datos.length; k++){
      if(datos[k] != undefined){
        this.j.push(datos[k]);
      }
    }
  }

  private formatPagination(totalPages: number): void {
    this.listPages = [];
    for(let i = 0; i < totalPages; i++){
      this.listPages.push(i);
    }
  }
  private createFilterParams(): void {
    this.filters.pageSize = this.sizePage;
    this.filters.pageNumber = this.page;
    
    if (this.searchForm.value.code) {
      this.filters['code'] = this.selectFichaTemp.code;
    } else {
      delete this.filters['code'];
    }

    if (this.selectApprenticeTemp) {
      this.filters['documentApprentice'] = this.selectApprenticeTemp.detailUser.documentNumber;
    } else {
      delete this.filters['documentApprentice'];
    }

    if (this.selectInstructorTemp) {
      this.filters['documentInstructor'] = this.selectInstructorTemp.detailUser.documentNumber;
    } else {
      delete this.filters['documentInstructor'];
    }
    if (this.searchForm.value.status) {
      this.filters['status'] = this.searchForm.value.status;
    } else {
      delete this.filters['status'];
    }

    if (this.searchForm.value.typeCallAttention) {
      this.filters['typeCallAttention'] = this.searchForm.value.typeCallAttention;
    } else {
      delete this.filters['typeCallAttention'];
    }
    if (this.searchForm.value.trimestre) {
      this.filters['trimestre'] = this.searchForm.value.trimestre;
    } else {
      delete this.filters['trimestre'];
    }
  }
  sendDatos(): number {
    return this.idDataUser;
  }
}
