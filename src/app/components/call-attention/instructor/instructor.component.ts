import { Component, OnInit } from '@angular/core';
import { IDetailUser } from '../../detail-user/detail-user-interface';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CallAttentionService } from '../call-attention.service';
import { ITeamFicha } from '../../users-has-ficha/teamFicha-interface';
import { IFicha } from '../../ficha/ficha-interface';
import { ICallAttention } from '../call-attention-interface';
import { ConfirmationService } from 'primeng/api';
import swal from 'sweetalert2';
import { AccountService } from 'src/app/auth/account.service';
import { IAccount } from 'src/app/auth/auth-shared/models/account.model';
import { FichaService } from '../../ficha/ficha.service';
import { DetailUserService } from '../../detail-user/detail-user.service';
import { ITeamFicha2 } from '../../users-has-ficha/teamFicha-interface copy';


@Component({
  selector: 'app-instructor',
  templateUrl: './instructor.component.html',
  styleUrls: ['./instructor.component.styl'],
  providers: [ConfirmationService]
})
export class InstructorComponent implements OnInit {
  apprentices: ITeamFicha;
  apprenticesList: ITeamFicha[];
  selectApprenticeTemp: ITeamFicha;
  selectDetailUserApprenticeTemp: IDetailUser;
  instructors: ITeamFicha;
  instructorsList: ITeamFicha[];

  selectDetailUserInstructorTemp: IDetailUser;
  selectFichaTemp: IFicha;
  saveAttentionCalle: ICallAttention[];
  callAttention: ICallAttention;
  callAttentionList: ICallAttention[];
  j:ITeamFicha2[] = [];
/**autoComplite */
  fichaList:IFicha[] = []; 
  selectFichaTemp2:IFicha;
  apprenticeList:ITeamFicha2[] = [];
  selectApprenticeTemp2:ITeamFicha2;
  /**autoComplite fin */

  attentionCalledView = 0;
  r = 0;
  userData: IAccount;
  /** Form Group*/
  selectedApprentice: number;
  UserFormGroup: FormGroup;
  /**End Form Group */
  detailUserList: IDetailUser[];
  detailUser: IDetailUser;
  existData = false;
  pageNumber = 0;
  pageSize = 10;

  filters = {
    code:'',
    documentNumber:'',
    pageSize: 10,
    pageNumber: 0
  };

  searchForm = this.fb.group({
    code: '',
    documentNumber: ''
  });
 

  constructor(private formBuilder: FormBuilder,
    private fb: FormBuilder,
    private callAttentionService: CallAttentionService,
    private accountService: AccountService,
    private confirmationService: ConfirmationService,
    private fichaService: FichaService,
    private detailUserService: DetailUserService) {



    this.UserFormGroup = this.formBuilder.group({
      typeDocument: ['', [
        Validators.required
      ]],
      documentNumber: ['', [
        Validators.required
      ]],
      name: ['', [
        Validators.required
      ]],
      description: ['', [
        Validators.required
      ]],
      activities: ['', [
        Validators.required
      ]],
      type: ['', [
        Validators.required
      ]],
      trimestre:['',[
        Validators.required
      ]]
    });
  }


  ngOnInit() {
    this.getUserData()
    this.  addToInstructor()

  }

  searchApprenticeAndInstructorByFicha() {
    this.apprenticesList = null;
    this.createFilterParams();
    this.callAttentionService.searchAppreticeByFicha(this.filters)
      .subscribe((res: any) => {
        this.apprenticesList = (res.content);
    this.callAttentionService.searchInstructorByFicha({
      'code': this.apprenticesList[0].codeFicha,
      'pageSize': 10,
      'pageNumber': 0
    })
      .subscribe((res: any) => {
        this.instructorsList = res.content;
      })

      this.callAttentionService.searchFichaByCode({
        'code': this.apprenticesList[0].codeFicha
      })
        .subscribe((res: any) => {
          this.selectFichaTemp = (res);
        })
      })
      

    

  }

  //busqueda de autoComplic para ficha
  searchFichaByCode(event: any): void {
    this.fichaService.searchFichaByCode({
      'code': event.query
    })
    .subscribe(res =>{
      this.fichaList = res;
    })
  }

  selectFichatemp2(ficha: IFicha):void{
    this.selectFichaTemp2 = (ficha);
    this.searchApprenticeAndInstructorByFicha();
  }

  //busqueda de autoComplic para aprendiz
  searchApprentice(event: any): void {
    this.j = [];
    this.detailUserService.searchUsers({
      'firstName': event.query
    })
    .subscribe(res =>{
      this.filterData(res);
      this.apprenticeList = (this.j);
    })
  }
  //seleccion de un aprendiz 
  selectAppreticeTemp(user: ITeamFicha2):void{
    this.selectApprenticeTemp2 = (user);
    this.searchApprenticeAndInstructorByFicha();
  }
  //seleccion de un aprendiz end
//busqueda de autoComplic para aprendiz end

  addToInstructor() {

    this.callAttentionService.getByDetailUser({
      'login': this.userData.login
    })
      .subscribe((res: any) => {

        this.selectDetailUserInstructorTemp = (res);
      })

  }

  viewAttentionCalled(apprentice: ITeamFicha) {
    this.r = 0;
    this.callAttentionList = [];
    this.selectApprenticeTemp = apprentice;
    this.callAttentionService.searchAppreticeCallAttention({
      'documentNumber': this.selectApprenticeTemp.documentNumber,
      'code': this.selectApprenticeTemp.codeFicha,
      'pageSize': 5,
      'pageNumber': 0
    })
      .subscribe((res: any) => {
        if(res.numberOfElements > 0) {
          this.callAttentionList = res.content;
        } else {
          swal.fire('¡OOPS!', 'El aprendiz ' + apprentice.fullName + ' no tiene llamados de atencion', 'error');
          this.display = false;
        }

      })

  }


  addToApprentice(apprentice: ITeamFicha) {
    this.r = 1;

    this.selectApprenticeTemp = apprentice;
    this.UserFormGroup = this.formBuilder.group({
      typeDocument: [this.selectApprenticeTemp.documentType, [
        Validators.required
      ]],
      documentNumber: [this.selectApprenticeTemp.documentNumber, [
        Validators.required
      ]],
      name: [this.selectApprenticeTemp.fullName, [
        Validators.required
      ]],
      description: ['', [
        Validators.required
      ]],
      activities: ['', [
        Validators.required
      ]],
      type: ['', [
        Validators.required
      ]],
      trimestre:['', 
    Validators.required]
    });

    this.callAttentionService.searchDetailUserById({
      'id': this.selectApprenticeTemp.id
    })
      .subscribe((res: any) => {

        this.selectDetailUserApprenticeTemp = (res);
        this.attentionCalledView = 1;

      })

  }
  saveAttentionCalled(): void {
    let call: ICallAttention = {
      situation: this.UserFormGroup.get("description").value,
      summary: this.UserFormGroup.get("activities").value,
      commitment: '',
      ficha: this.selectFichaTemp[0],
      aprendiz: this.selectDetailUserApprenticeTemp,
      instructor: this.selectDetailUserInstructorTemp,
      status: true,
      typeCallAttention: this.UserFormGroup.get("type").value,
      callAttentionDate: '',
      trimestre: this.UserFormGroup.value.trimestre
    }
    
    swal.fire('Llamado de atención', 'Se esta creando el llamado de atención...', 'warning');
    this.callAttentionService.saveAttentionCalled(call)
      .subscribe((res: any) => {
        swal.fire('Llamado de atención', 'Se creo exitosamente el llamado de atención', 'success');
        this.attentionCalledView = 0;
        this.displayMaximizable = false;
      },
      (error: any) => {
        swal.fire('¡OOPS!', 'No se pudo guardar satisfactoriamente tu llamado de atencion', 'error');
      } );
  }

  //traer los datos
  getUserData() {
    /**AccountService*/
    this.accountService.getUserData()
    this.userData = this.accountService.user;
  }
  confirm() {
    this.displayMaximizable = false;
    this.confirmationService.confirm({
      message: '¿Desea confirmar el envío del formulario?',
      accept: () => {
        this. saveAttentionCalled()
      },
      reject: () => {
        this.displayMaximizable = true;
      }
    });
  }

 //modal primeNg
    display: boolean;
    displayMaximizable: boolean;
    showDialog() {
        this.display = true;
    }
    showMaximizableDialog() {
      this.displayMaximizable = true;
  }
//modal primeNg fin


  private createFilterParams(): void {
    this.filters.pageSize = 10;
    this.filters.pageNumber = 0;
    
    if (this.selectFichaTemp2) {
      this.filters['code'] = this.selectFichaTemp2.code;
    } else {
      delete this.filters['code'];
    }

    if (this.selectApprenticeTemp2) {
      this.filters['documentNumber'] = this.selectApprenticeTemp2.detailUser.documentNumber;
    } else {
      delete this.filters['documentNumber'];
    }

 }

 
 private filterData(datos:ITeamFicha2[] = []): void {
  let contador = 0;
  let validar = 0;

  for(var g = 0; g < datos.length; g++){
    
    for(var s = 1; s < datos.length; s++){
      if(g != s){
        if(datos[g].detailUser.documentNumber == datos[s].detailUser.documentNumber){
          validar++;
        }
      }
      
    }
  }

  if(validar != 0){
    for(let i = 0; i < datos.length; i++){
      contador = 0;
      for(let j = 1; j < datos.length; j++){
        while(datos[j] == undefined && j < datos.length || datos[i].id == datos[j].id && j < datos.length){
          j += 1;
          if(j >= datos.length){
            break;
          
          }
          
        }
        if(datos[j] != undefined){
          if(datos[i].detailUser.documentNumber == datos[j].detailUser.documentNumber){;
            delete datos[j];
            contador++;
          } 
        }
      
        
      }
      if(contador != 0){
        i += contador;
      }
    }
  }
  
  for(let k = 0; k < datos.length; k++){
    if(datos[k] != undefined){
      this.j.push(datos[k]);
    }
  }
  
}

}
