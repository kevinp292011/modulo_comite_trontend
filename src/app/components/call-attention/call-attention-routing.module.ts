import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AprendizComponent } from './aprendiz/aprendiz.component';
import { InstructorComponent } from './instructor/instructor.component';
import { InstructorListComponent } from './instructor-list/instructor-list.component';
import { InstructorUpdateComponent } from './instructor-update/instructor-update.component';
import { AprendizListComponent } from './aprendiz-list/aprendiz-list.component';
import { Authority } from 'src/app/auth/auth-shared/constants/authority.constants';
import { UserRouteAccesService } from 'src/app/auth/guards/user-route-acces.service';


const routes: Routes = [
  
{
  path:"aprendiz-call-attention/:id",
  data: {
    authorities: [Authority.ADMIN, Authority.APRENDIZ]
  },
  canActivate: [UserRouteAccesService],
  
  component: AprendizComponent
},
{
  path:"instructor-call-attention",
  data: {
    authorities: [Authority.ADMIN, Authority.INSTRUCTOR]
  },
  canActivate: [UserRouteAccesService],
  component: InstructorComponent
},
{
  path:"instructor-call-attentionList",
  data: {
    authorities: [Authority.ADMIN, Authority.INSTRUCTOR]
  },
  canActivate: [UserRouteAccesService],
  component: InstructorListComponent
},
{
  path:"instructor-call-attentionUpdate/:id",
  data: {
    authorities: [Authority.ADMIN, Authority.INSTRUCTOR]
  },
  canActivate: [UserRouteAccesService],
  component: InstructorUpdateComponent
},
{
  path:"aprendiz-call-attention-list",
  data: {
    authorities: [Authority.ADMIN, Authority.APRENDIZ]
  },
  canActivate: [UserRouteAccesService],
  component:AprendizListComponent 
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CallAttentionRoutingModule { }
