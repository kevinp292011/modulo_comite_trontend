import { IFicha } from '../ficha/ficha-interface';
import { IDetailUser } from '../detail-user/detail-user-interface';
export interface ICallAttention{
    id?:number;   
    situation?:string;
    summary?:string;
    impact?:string;
    efect?:string;
    commitment?:string;
    ficha?: IFicha;
    aprendiz?:IDetailUser;
    instructor?: IDetailUser;
    status?:Boolean;
    typeCallAttention?:string;
    callAttentionDate?:string;
    trimestre?: number;
}