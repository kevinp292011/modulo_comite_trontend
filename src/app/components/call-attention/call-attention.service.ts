import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ICallAttention } from './call-attention-interface';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ITeamFicha } from '../users-has-ficha/teamFicha-interface';
import { createRequestOption } from '../../utils/request_utils';
import { IFicha } from '../ficha/ficha-interface';
import { IDetailUser } from '../detail-user/detail-user-interface';
import { ITeamFicha2 } from '../users-has-ficha/teamFicha-interface copy';

@Injectable({
    providedIn: 'root'
  })
  export class CallAttentionService {
  
    constructor(private http: HttpClient) { }
  
    public searchAppreticeByFicha(req?: any): Observable<ITeamFicha[]> {
      let params = createRequestOption(req);

      return this.http.get<ITeamFicha[]>(`${environment.END_POINT}/api/teamFicha/getByUserFicha`,{params: params})
      .pipe(map(res => {
        return res;
      }));
    }
    public searchInstructorByFicha(req?: any): Observable<ITeamFicha[]> {
      let params = createRequestOption(req);

      return this.http.get<ITeamFicha[]>(`${environment.END_POINT}/api/teamFicha/getByInstructorFicha`,{params: params})
      .pipe(map(res => {
        return res;
      }));
    }
    public searchFichaByCode(req?: any): Observable<IFicha> {
      let params = createRequestOption(req);

      return this.http.get<IFicha>(`${environment.END_POINT}/api/ficha/search`,{params: params})
      .pipe(map(res => {
        return res;
      }));
    }
    public searchFichaByFirstName(req?: any): Observable<ITeamFicha> {
      let params = createRequestOption(req);

      return this.http.get<ITeamFicha>(`${environment.END_POINT}/api/teamFicha/search-Apprentice`,{params: params})
      .pipe(map(res => {
        return res;
      }));
    }
    public searchDetailUserById(req?: any): Observable<IDetailUser> {
      let params = createRequestOption(req);

      return this.http.get<IDetailUser>(`${environment.END_POINT}/api/detailUser/search/{id}`,{params: params})
      .pipe(map(res => {
        return res;
      }));
    }
   
    public searchAppreticeCallAttention(req?: any): Observable<ICallAttention> {
      let params = createRequestOption(req);

      return this.http.get<ICallAttention>(`${environment.END_POINT}/api/attention-called/get-by-user`,{params: params})
      .pipe(map(res => {
        return res;
      }));
    }

    public saveAttentionCalled(attentionCallen: ICallAttention): Observable<ICallAttention> {
      return this.http.post<ICallAttention>(`${environment.END_POINT}/api/attentionCalled`,attentionCallen)
      .pipe(map(res => {
        return res;
      }));
    }

    public searchCallAttentionFilter(req?: any): Observable<ICallAttention> {
      let params = createRequestOption(req);

      return this.http.get<ICallAttention>(`${environment.END_POINT}/api/attention-called/get-by-filterCallAttention`,{params: params})
      .pipe(map(res => {
        return res;
      }));
    }
    
    public updateCallAttention(attentionCallen: ICallAttention): Observable<ICallAttention> {
      return this.http.put<ICallAttention>(`${environment.END_POINT}/api/attentionCalled`, attentionCallen)
      .pipe(map(res => {
        return res;
      }));
    }
    public diligenciamientoCallAttention(attentionCallen: ICallAttention): Observable<ICallAttention> {
      return this.http.put<ICallAttention>(`${environment.END_POINT}/api/attentionCalled-email`, attentionCallen)
      .pipe(map(res => {
        return res;
      }));
    }
    public getCallAttentionById(id : any): Observable<ICallAttention> {
      let params = createRequestOption(id);
      
      return this.http.get<ICallAttention>(`${environment.END_POINT}/api/attention-called/search`,{params:params})
      .pipe(map(res => {
        return res;
      }));
    }
    public updateAttentionCall(attentionCallen: ICallAttention): Observable<ICallAttention> {
      return this.http.put<ICallAttention>(`${environment.END_POINT}/api/attentionCalled`, attentionCallen)
      .pipe(map(res => {
        return res;
      }));
    }
    public getByDetailUser(login : any): Observable<IDetailUser> {
      let params = createRequestOption(login);
      
      return this.http.get<IDetailUser>(`${environment.END_POINT}/api/detailUser/search/login`,{params:params})
      .pipe(map(res => {
        return res;
      }));
    }
    public getByTeamFichaByLogin(login : any): Observable<ITeamFicha2> {

      let params = createRequestOption(login);
      return this.http.get<ITeamFicha2>(`${environment.END_POINT}/api/teamFicha/search/login`,{params:params})
      .pipe(map(res => {
        return res;
      }));
    }

  }
  