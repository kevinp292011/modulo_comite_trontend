import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { ConfirmationService } from 'primeng/api';
import { CallAttentionService } from '../call-attention.service';
import { ICallAttention } from '../call-attention-interface';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';



@Component({
  selector: 'app-aprendiz',
  templateUrl: './aprendiz.component.html',
  styleUrls: ['./aprendiz.component.styl'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: { showError: true }
  },
  [ConfirmationService]
  ]
})
export class AprendizComponent implements OnInit {
  callAttention: ICallAttention;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  fifthFormGroup: FormGroup;
  sixthFormGroup: FormGroup;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private confirmationService: ConfirmationService,
    private _formBuilder: FormBuilder,
    private callAttentionService: CallAttentionService) {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required],
      id: [],
      ficha: [],
      aprendiz: [],
      instructor: [],
      status: [],
      typeCallAttention: [],
      callAttentionDate: []
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['',
        Validators.required
      ],
    });
    this.thirdFormGroup = this._formBuilder.group({
      thirdCtrl: ['',
        Validators.required,
      ]
    });
    this.fourthFormGroup = this._formBuilder.group({
      fourthCtrl: ['',
        Validators.required,
      ]
    });
    this.fifthFormGroup = this._formBuilder.group({
      fifthCtrl: ['',
        Validators.required
      ]
    });
  }

  ngOnInit(): void {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    this.callAttentionService.getCallAttentionById({
      'id': id
    })
      .subscribe(res => {
        this.callAttention = (res);
        this.loadForm(this.callAttention);
      });
  }

  updateCallAttention() {
    let call: ICallAttention = {
      id: this.callAttention.id,
      situation: this.callAttention.situation,
      summary: this.callAttention.summary,
      commitment: this.fifthFormGroup.get("fifthCtrl").value,
      ficha: this.callAttention.ficha,
      aprendiz: this.callAttention.aprendiz,
      instructor: this.callAttention.instructor,
      status: false,
      typeCallAttention: this.callAttention.typeCallAttention,
      callAttentionDate: this.callAttention.callAttentionDate,
      impact:this.thirdFormGroup.get("thirdCtrl").value,
      efect:this.fourthFormGroup.get("fourthCtrl").value,
      trimestre: this.callAttention.trimestre
    } 
    Swal.fire('Llamado de atención', 'Se esta creando el llamado de atención...', 'warning');
    this.callAttentionService.diligenciamientoCallAttention(call)
      .subscribe(res => {
        this.router.navigate(['../dashboard/call-attention/aprendiz-call-attention-list']); // Redireccionar
        Swal.fire('Llamado de atención', 'Se creo exitosamente el llamado de atención', 'success');
      });
  }

  private loadForm(callAttention: ICallAttention) {
    this.firstFormGroup.patchValue({
      firstCtrl: callAttention.situation,
      id: callAttention.id,
      ficha: callAttention.ficha,
      aprendiz: callAttention.aprendiz,
      instructor: callAttention.instructor,
      status: callAttention.status,
      typeCallAttention: callAttention.typeCallAttention,
      callAttentionDate: callAttention.callAttentionDate
    }),
      this.secondFormGroup.patchValue({
        secondCtrl: callAttention.summary
      }),
      this.thirdFormGroup.patchValue({
        thirdCtrl: callAttention.impact
      }),
      this.fourthFormGroup.patchValue({
        fourthCtrl: callAttention.efect
      }),
      this.fifthFormGroup.patchValue({
        fifthCtrl: callAttention.commitment
      })
  }

  confirm() {
    this.confirmationService.confirm({
      message: '¿Desea confirmar el envío del formulario?',
      accept: () => {
        this.updateCallAttention()
      },
      reject: () => {
      }
    });
  }
}


