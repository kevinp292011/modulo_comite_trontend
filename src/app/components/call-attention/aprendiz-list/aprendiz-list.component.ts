import { Component, OnInit } from '@angular/core';
import { CallAttentionService } from '../call-attention.service';
import { FormBuilder } from '@angular/forms';
import { ITeamFicha } from '../../users-has-ficha/teamFicha-interface';
import { ICallAttention } from '../call-attention-interface';
import swal from 'sweetalert2';
import { AccountService } from 'src/app/auth/account.service';
import { IAccount } from 'src/app/auth/auth-shared/models/account.model';
import { ITeamFicha2 } from '../../users-has-ficha/teamFicha-interface copy';

@Component({
  selector: 'app-aprendiz-list',
  templateUrl: './aprendiz-list.component.html',
  styleUrls: ['./aprendiz-list.component.styl']
})
export class AprendizListComponent implements OnInit {

  apprenticeList: ITeamFicha[];
  selectApprenticeTemp: ITeamFicha2;
  callAttentionList: ICallAttention[];
  sizePage = 3;
  listPages = [];
  page: number;
  contador = 0;
  r = 0;
  userData: IAccount;


  searchForm = this.formBuilder.group({
    code: '',
    documentNumber: ''
  });
  filters = {
    code: '',
    documentNumber: '',
    pageSize: this.sizePage,
    pageNumber: 1
  };

  constructor(private formBuilder: FormBuilder, private accountService: AccountService, private callAttentionService: CallAttentionService) { }

  ngOnInit(): void {
    this.Paginacion(0);
    this.getUserData();
    this.addToApprentice();
    this.Paginacion2(0);
   
  }
  Paginacion(page1: number) {

    this.page = 0;
    this.page = (page1);
    if (this.contador != 0) {
      this.searchApprenticeByFichaAndDocumentNumber();
    }
  }
  Paginacion2(page2: number) {
    this.page = (page2);
    this.addToApprentice();

  }
  searchApprenticeByFichaAndDocumentNumber() {
    this.contador++;
    this.apprenticeList = [];
    this.callAttentionList = null;
    this.createFilterParams();
    this.callAttentionService.searchAppreticeByFicha(this.filters)
      .subscribe((res: any) => {
        this.apprenticeList = res.content;
        this.formatPagination(res.totalPages);
        this.page = 0;
      }, err => { })

  }

  addToApprentice() {
    this.r = 1;

    this.callAttentionService.getByTeamFichaByLogin({ 'login': this.userData.login })
      .subscribe(res => {
        this.selectApprenticeTemp = (res);
        this.callAttentionService.searchAppreticeCallAttention({
          'documentNumber': this.selectApprenticeTemp.detailUser.documentNumber,
          'code': this.selectApprenticeTemp.ficha.code,
          'pageSize': this.sizePage,
          'pageNumber': this.page
        })
          .subscribe((res: any) => {
            if (res.numberOfElements > 0) {
              this.callAttentionList = res.content;
              this.formatPagination(res.totalPages);
            } else {
              swal.fire('¡OOPS!', ' No se encontraron llamados de atencion  llamados de atencion', 'error');

            }

          })

      })





  }
  getUserData() {
    /**AccountService*/
    this.accountService.getUserData()
    this.userData = this.accountService.user;
  }

  private formatPagination(totalPages: number): void {
    this.listPages = [];
    for (let i = 0; i < totalPages; i++) {
      this.listPages.push(i);
    }
  }
  private createFilterParams(): void {
    this.filters.pageSize = this.sizePage;
    this.filters.pageNumber = this.page;

    if (this.searchForm.value.code) {
      this.filters['code'] = this.searchForm.value.code;
    } else {
      delete this.filters['code'];
    }

    if (this.searchForm.value.documentNumber) {
      this.filters['documentNumber'] = this.searchForm.value.documentNumber;
    } else {
      delete this.filters['documentNumber'];
    }
  }

}
