import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AprendizListComponent } from './aprendiz-list.component';

describe('AprendizListComponent', () => {
  let component: AprendizListComponent;
  let fixture: ComponentFixture<AprendizListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AprendizListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AprendizListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
