export interface IAccount {
    activated: boolean,
    authorities: string[],
    email: string,
    firstName: string,
    lastName: string,
    login: string,
    id: number
}



export class Account {
    constructor(
        public activated: boolean,
        public authorities: string[],
        public email: string,
        public firstName: string,
        public lastName: string,
        public login: string,
        public id: number
    ) { }
}