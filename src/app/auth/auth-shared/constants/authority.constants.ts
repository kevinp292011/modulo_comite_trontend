export const enum Authority{
    APRENDIZ = 'APRENDIZ',
    ADMIN = 'ADMIN',
    INSTRUCTOR = 'INSTRUCTOR',
    ASPIRANTE = 'ASPIRANTE'
}