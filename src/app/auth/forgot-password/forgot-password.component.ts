import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AccountService } from '../account.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.styl']
})
export class ForgotPasswordComponent implements OnInit {
  userEmail: FormGroup;
  email: string;
  constructor(private accountService: AccountService, private router: Router, private fb: FormBuilder) {
    this.userEmail = this.fb.group({
      email: ['', [Validators.required,
      Validators.email,
      Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")
      ]]
    });

  }

  ngOnInit(): void {
  }


  onReset() {

    this.email = this.userEmail.get("email").value;
    this.accountService.forgotPassword(this.email)
      .subscribe((res: any) => {
        Swal.fire('OKEY', 'Revise su correo electrónico para obtener más información sobre cómo restablecer su contraseña.', 'success');
        this.router.navigate(['/login']);
      },
        (error: any) => {
          Swal.fire('OOPS', 'El correo que ingresaste no se encuentra registrado.', 'error');
        });
  }
}
