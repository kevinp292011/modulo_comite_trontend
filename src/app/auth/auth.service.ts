import { Injectable } from '@angular/core';
import { ICredentials } from './auth-shared/models/credentials';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators'
import { environment } from 'src/environments/environment';
import { LoginService } from './login/login.service';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }
  public login(credentials: ICredentials): Observable<any>{
    
    const data: ICredentials = {
      username: credentials.username,
      password: credentials.password,
      rememberMe: credentials.rememberMe
    }
    /**Nuevo */
    const credenciales = btoa('inventory' + ':' + '12345');
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Basic ' + credenciales
    });
    let params = new URLSearchParams();
    params.set('grant_type', 'password');
    params.set('username', credentials.username);
    params.set('password', credentials.password);
    return this.http.post<any>(`${environment.END_POINT}/oauth/token`, params.toString(), {headers: httpHeaders})
    .pipe(map(res =>{     
      const token = res.access_token;
      if (token && token.length > 0) {
        this.storeAuthenticationToken(token, credentials.rememberMe);
        return token;
      }
     /* if (bearerToken && bearerToken.slice(0, 7) === 'Bearer '){
        const jwt = bearerToken.slice(7, bearerToken.length);
        this.storeAuthenticationToken(jwt, credentials.rememberMe)
        
        return jwt;
      }*/
    }));
  }

  logout(): Observable<any>{
    return new Observable(observe => {
      localStorage.removeItem('token')
      sessionStorage.removeItem('token');
      observe.complete();
    })
  }

  private storeAuthenticationToken(jwt: string, rememberMe: boolean): void{
    if(rememberMe){
      localStorage.setItem('token', jwt);
    }else{
      sessionStorage.setItem('token',  jwt);
    }
  }
}