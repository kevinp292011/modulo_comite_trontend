import { Injectable } from '@angular/core';
import { ReplaySubject, Observable, of } from 'rxjs';
import { Account, IAccount } from './auth-shared/models/account.model';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { catchError, shareReplay, tap, map } from 'rxjs/operators';
import { Authority } from './auth-shared/constants/authority.constants';
import { ICredentials } from './auth-shared/models/credentials';
import { error } from 'protractor';
import { createRequestOption } from '../utils/request_utils';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  private authenticationState = new ReplaySubject<Account | null>(1);
  private userIdentity: Account | null = null;
  private accountCache?: Observable<Account | null>;
  user: IAccount;
  constructor(
    private http: HttpClient, private router: Router
  ) { }


  /**
   * Create User
   * @param account  
   */

  create(credentials: ICredentials): Observable<ICredentials> {
    return this.http.post<ICredentials>(`${environment.END_POINT}/oaut/token`, credentials);
  }
  /* Create User*/
  createUser(user: IAccount): Observable<IAccount> {
    return  this.http.post<IAccount>(`${environment.END_POINT}/api/users`, user )
      .pipe(map((res) => {
        return res;
      }, error => {
        return error;
      }));
  }

  getUserData() {
    this.user = this.userIdentity;
    return this.user;
  }

  forgotPassword(email: string): Observable<any> {
    return  this.http.post<any>(`${environment.END_POINT}/api/account/reset-password/init`, email )
      .pipe(map((res) => {
        return res;
      }, error => {
        return error;
      }));
  }
  identity(force?: boolean): Observable<Account | null> {
    if (!this.accountCache || force || !this.isAuthenticated()) {
      this.accountCache = this.fetch().pipe(catchError(() => {
        return of(null);
      }), tap((account: Account | null) => {
        this.authenticate(account);

      }), shareReplay());
    }

    return this.accountCache;
  }

  authenticate(identity: Account | null): void {
    this.userIdentity = identity;
  }

  isAuthenticated(): boolean {
    return this.userIdentity !== null;
  }

  getUserName(): string {
    return this.userIdentity.firstName + ' ' + this.userIdentity.lastName;
  }

  getAuthenticationState(): Observable<Account | null> {
    return this.authenticationState.asObservable();
  }

  /*
   * @param authorities verify rols to users
   */
  hasAnyAuthority(authorities: string[] | string): boolean {
    if (!this.userIdentity || !this.userIdentity.authorities) {
      return false;
    }
    if (!Array.isArray(authorities)) {
      authorities = [authorities];
    }
    return this.userIdentity.authorities.some((authority: string) => authorities.includes(authority));
  }

  private fetch(): Observable<Account> {
    const jwt = localStorage.getItem('token') || sessionStorage.getItem('token');
    if (jwt) {
    const payload: any = JSON.parse(atob(jwt.split('.')[1]));
    const params = createRequestOption({login: payload.user_name});
    return this.http.get<Account>(`${environment.END_POINT}/api/user/login`, {params:params});
    }
  }
}