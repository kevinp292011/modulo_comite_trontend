import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ICredentials, Credentials } from '../auth-shared/models/credentials';
import { Router } from '@angular/router';
import { LoginService } from './login.service';
import { AccountService } from '../account.service';
import { ConfirmationService } from 'primeng/api';
import { IAccount } from '../auth-shared/models/account.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.styl'],
  providers: [ConfirmationService]
})
export class LoginComponent implements OnInit {

  hide = true
  user: IAccount;
  saved = false;
  roles = [] = [];
  loginForm: FormGroup;
  singUp: FormGroup;


  constructor(
    private fb: FormBuilder,
    private loginService: LoginService,
    private accountService: AccountService,
    private router: Router,
    private confirmationService: ConfirmationService) {

    this.loginForm = this.fb.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
      rememberMe: ['']
    });

    this.singUp = this.fb.group({
      login: ['', [Validators.required]],
      firstName: [''],
      lastName: ['', [Validators.required]],
      email: ['', [Validators.required]],
      authorities: ['', [Validators.required]]

    });
  }
  ngOnInit(): void {
  }

  /*Login Function* */
  login(): void {
    this.loginService.logout();
    const credentials: ICredentials = new Credentials();
    credentials.username = this.loginForm.value.username;
    credentials.password = this.loginForm.value.password;
    credentials.rememberMe = this.loginForm.value.rememberMe;

    this.loginService.login(credentials)
      .subscribe((res: any) => {
        this.router.navigate(['/dashboard/dashboard-home']);
        Swal.fire('¡Bienvenido!', 'Se ha iniciado sesión correctamente', 'success');
      },
        (error: any) => {
          this.confirm()
        });
  }
  /**Modal of login*/
  confirm() {
    this.confirmationService.confirm({
      message: 'Usuario y contraseña invalidos. Intente nuevamente',
      accept: () => {

      }
    });
  }
}











