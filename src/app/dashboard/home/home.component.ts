import { Component, OnInit } from '@angular/core';
import {MenuItem} from 'primeng/api';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.styl']
})
export class HomeComponent implements OnInit {
  static readonly ROUTE_DATA_BREADCRUMB = 'breadcrumb';
  breadcrumbItem: MenuItem[] = [];
  constructor(private router: Router,  private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      this.breadcrumbItem.push(data.breadcrumb);
    });

  }

}
