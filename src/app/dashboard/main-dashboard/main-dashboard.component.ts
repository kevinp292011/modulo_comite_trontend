import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-dashboard',
  templateUrl: './main-dashboard.component.html',
  styleUrls: ['./main-dashboard.component.styl']
})
export class MainDashboardComponent implements OnInit {
  isActive = false;
  constructor() { }

  ngOnInit() {
  }
  
  cambiar(valor: boolean): void {
    this.isActive = valor;
  }

  sendIsActivate(): boolean {
    return this.isActive;
  }

}
