import { Component, OnInit, Input } from '@angular/core';
import { FormComplaintsComponent } from '../form-complaints/form-complaints.component';
import { MatDialog } from '@angular/material/dialog';
import { ModalUserComponent } from '../modal-user/modal-user.component';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.styl']
})
export class SidebarComponent implements OnInit {
  opened = true
  
  @Input() isActivated: boolean; 

  constructor(public dialog: MatDialog) { }
  /**Modal Form Complaints*/
  openDialog() {
    this.dialog.open(FormComplaintsComponent);
  }
    /**Modal Form users*/
  openDialogA() {
    this.dialog.open(ModalUserComponent);
  }

  ngOnInit() {
  }

}
