export interface IBuzon {
    id?:number;
    name?:string;
    email?:string;
    telefono?:string;
    fecha?:string;
    sugerencia?:string;
}
