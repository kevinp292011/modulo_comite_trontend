import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import {DashboardService} from '../dashboard.service'
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { ITeamFicha2 } from 'src/app/components/users-has-ficha/teamFicha-interface copy';
import { IAccount } from 'src/app/auth/auth-shared/models/account.model';
import { AccountService } from 'src/app/auth/account.service';
import { CallAttentionService } from 'src/app/components/call-attention/call-attention.service';
@Component({
  selector: 'app-form-complaints',
  templateUrl: './form-complaints.component.html',
  styleUrls: ['./form-complaints.component.styl']
})
export class FormComplaintsComponent implements OnInit {
  buzon:FormGroup;
  userData: IAccount;
  selectDetailUserTemp:ITeamFicha2;
  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private buzonService: DashboardService,
    private accountService: AccountService,
    private callAttentionService: CallAttentionService
    ) { 
    this.buzon = this.formBuilder.group({
      name: ['', [
        Validators.required
      ]],
      email: ['', [
        Validators.required
      ]],
      telefono: ['', [
        Validators.required
      ]],
      sugerencia: ['', [
        Validators.required
      ]]
    })
  }

  ngOnInit(): void {
    this.getUserData()
  }

  sendSugerencia(){
    this.buzonService.saveSugerencia(this.buzon.value)
      .subscribe((res: any) => {
        Swal.fire('¡Gracias!', 'Pronto nos pondrémos en contácto', 'success');
      },
      (error: any) => {
        Swal.fire('¡OOPS!', 'No se pudo guardar satisfactoriamente tu sugerencia', 'error');
      });

  }

  getUserData() {
    /**AccountService*/
    this.accountService.getUserData()
    this.userData = this.accountService.user;

        this.callAttentionService.getByTeamFichaByLogin({'login': this.userData.login})
        .subscribe((res:any) =>{
          this.selectDetailUserTemp = res;

          this.buzon = this.formBuilder.group({
            name: [this.selectDetailUserTemp.detailUser.users.lastName, [
              Validators.required
            ]],
            email: [this.selectDetailUserTemp.detailUser.emailPersonal, [
              Validators.required
            ]],
            telefono: [this.selectDetailUserTemp.detailUser.phone, [
              Validators.required
            ]],
            sugerencia: ['', [
              Validators.required
            ]]
          })

        })
        
       
      
  }

}
