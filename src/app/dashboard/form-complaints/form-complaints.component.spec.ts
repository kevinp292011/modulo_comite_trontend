import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormComplaintsComponent } from './form-complaints.component';

describe('FormComplaintsComponent', () => {
  let component: FormComplaintsComponent;
  let fixture: ComponentFixture<FormComplaintsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormComplaintsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormComplaintsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
