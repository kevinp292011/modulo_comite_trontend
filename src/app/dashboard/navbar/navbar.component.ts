import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { AccountService } from 'src/app/auth/account.service';
import { ConfirmationService } from 'primeng/api';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { MatDialog } from '@angular/material/dialog';
import { ProfileUserComponent } from '../profile-user/profile-user.component';
import { IAccount } from 'src/app/auth/auth-shared/models/account.model';
import { CallAttentionService } from 'src/app/components/call-attention/call-attention.service';
import { IDetailUser } from 'src/app/components/detail-user/detail-user-interface';
import { ITeamFicha2 } from 'src/app/components/users-has-ficha/teamFicha-interface copy';
import { ICallAttention } from 'src/app/components/call-attention/call-attention-interface';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.styl'],
  providers: [ConfirmationService]
})
export class NavbarComponent implements OnInit {
  fullName: string;
  isActive = false;
  userData: IAccount;
  selectDetailUserTemp: ITeamFicha2;
  listCallAttention: ICallAttention[];
  notifications = 0;

  @Output() openEvent = new EventEmitter<boolean>();

  constructor(
    public dialog: MatDialog,
    private router: Router,
    private authService: AuthService,
    private accountService: AccountService,
    private ConfirmationService: ConfirmationService,
    private callAttentionService: CallAttentionService
  ) { }

  /** Method for Open profile */
  openDialog() {
    this.dialog.open(ProfileUserComponent);
  }
  /** Method for Open profile */

  ngOnInit(): void {
    /**Method for get userName */
    this.fullName = this.accountService.getUserName();
    /**Method for get userName */
    this.getUserData();
  }

  getUserData() {
    /**AccountService*/
    this.accountService.getUserData()
    this.userData = this.accountService.user;

    /** DetailUsers*/

    this.callAttentionService.getByTeamFichaByLogin({ 'login': this.userData.login })
      .subscribe((res: any) => {
        this.selectDetailUserTemp = res;

        this.callAttentionService.searchAppreticeCallAttention({
          'documentNumber': this.selectDetailUserTemp.detailUser.documentNumber,
          'code': this.selectDetailUserTemp.ficha.code,
          'pageSize': 100,
          'pageNumber': 0
        })
          .subscribe((res: any) => {
            this.listCallAttention = res.content;
            if (res.numberOfElements > 0) {
              for (let i = 0; i < res.numberOfElements; i++) {
                if (this.listCallAttention[i].status == true) {
                  this.notifications++;
                }
              }
            }

          })

      })

  }

  /**Method for aside toggle */
  changeEvent(): void {
    this.isActive = !this.isActive;
    this.openEvent.emit(this.isActive);
  }
  /**Method for aside toggle */

  /**Method for close session */
  public cerrarSesion(): void {
    this.router.navigate(['login'])
    this.authService.logout().subscribe(null, null, () => this.accountService.authenticate(null));
    Swal.fire('¡Correcto!', 'Se ha cerrado sesion correctamente, esperamos regreses pronto', 'success');
  }
  /**Method for close session */

  /**Method for confirm dialog close session */
  confirm() {
    this.ConfirmationService.confirm({
      message: '¿Seguro que deseas cerrar sesion?',
      accept: () => {
        this.cerrarSesion();

      },
      reject: () =>{
      } 
    })
  }

  /**Method for confirm dialog close session */



}
