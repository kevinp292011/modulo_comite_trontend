import { Component, OnInit } from '@angular/core';
import { IAccount } from 'src/app/auth/auth-shared/models/account.model';
import { AccountService } from 'src/app/auth/account.service';
import { DetailUserService } from 'src/app/components/detail-user/detail-user.service';
import { IDetailUser } from 'src/app/components/detail-user/detail-user-interface';
import { CallAttentionService } from 'src/app/components/call-attention/call-attention.service';

@Component({
  selector: 'app-profile-user',
  templateUrl: './profile-user.component.html',
  styleUrls: ['./profile-user.component.styl']
})
export class ProfileUserComponent implements OnInit {
  selectDetailUserInstructorTemp: IDetailUser;
  userData: IAccount;

  constructor(
    private accountService: AccountService,
    private callAttentionService: CallAttentionService
    
    ) { }

  ngOnInit(): void {
    this.getUserData();
  }


  getUserData() {
    /**AccountService*/
    this.accountService.getUserData()
    this.userData = this.accountService.user;
    /** DetailUsers*/
    this.callAttentionService.getByDetailUser({
      'login': this.userData.login
    })
      .subscribe((res: any) => {

        this.selectDetailUserInstructorTemp = res;
      })
  }

}
