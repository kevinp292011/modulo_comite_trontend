import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainDashboardComponent } from './main-dashboard/main-dashboard.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { FooterComponent } from './footer/footer.component';
import { dashboardRoutingModule } from './dashboard-routing.module';
import { NavbarComponent } from './navbar/navbar.component';
import { ProgramModule } from '../components/program/program.module';
import { CallAttentionModule } from '../components/call-attention/call-attention.module';
import { MaterialModule } from '../material.module';
import { HomeComponent } from './home/home.component';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { FormComplaintsComponent } from './form-complaints/form-complaints.component';
import { ProfileUserComponent } from './profile-user/profile-user.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthSharedModule } from '../auth/auth-shared/auth-shared.module';
import { ModalUserComponent } from './modal-user/modal-user.component'


@NgModule({
  declarations: [MainDashboardComponent, SidebarComponent, FooterComponent, NavbarComponent, HomeComponent, FormComplaintsComponent, ProfileUserComponent, ModalUserComponent],
  imports: [
    CommonModule,
    dashboardRoutingModule,
    ProgramModule,
    CallAttentionModule,
    MaterialModule,
    BreadcrumbModule,
    ConfirmDialogModule,
    ReactiveFormsModule,
    AuthSharedModule
  ]

})
export class DashboardModule {
  constructor() {


  }
}
