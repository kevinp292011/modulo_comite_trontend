import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IBuzon } from '../dashboard/buzon';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IDetailUser } from '../components/detail-user/detail-user-interface';
import { createRequestOption } from '../utils/request_utils';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient) { }

  public saveSugerencia(buzon: IBuzon): Observable<IBuzon> {
    return this.http.post<IBuzon>(`${environment.END_POINT}/api/buzon`, buzon)
      .pipe(map(res => {
        return res;
      }));
  }
  public searchDetailUserById(req?: any): Observable<IDetailUser> {
    let params = createRequestOption(req);

    return this.http.get<IDetailUser>(`${environment.END_POINT}/api/detailUser/search/{id}`, { params: params })
      .pipe(map(res => {
        return res;
      }));
  }

}
