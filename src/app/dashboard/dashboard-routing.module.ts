import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainDashboardComponent } from './main-dashboard/main-dashboard.component';
import { HomeComponent } from './home/home.component';
import { Authority } from '../auth/auth-shared/constants/authority.constants';
import { UserRouteAccesService } from '../auth/guards/user-route-acces.service';

// dashboard/ficha/ficha-list

const routes: Routes = [
  {
    path: "",
    /** 
    data: {
      authorities: [Authority.ADMIN, Authority.APRENDIZ, Authority.INSTRUCTOR, Authority.ASPIRANTE]
    },
    canActivate: [UserRouteAccesService],
    */
    
    component: MainDashboardComponent,
    children: [
      /** Lazy Loading full */
    
      /**ROUTES FOR HOME**/
      {
        path: "dashboard-home",
        data: {
          breadcrumb: {
            label: "home",
            url: "dashboard-home"
          }
        },
        component: HomeComponent
      },
      /**END ROUTES FOR HOME**/
      /**ROUTES FOR ADMIN**/
      {
        path:"user",
        data: {
          authorities: [Authority.ADMIN]
        },
        canActivate: [UserRouteAccesService],
        
        loadChildren:()=> import('../components/users/users.module')
        .then(m => m.UsersModule)
      },
      {
        path:"detail-user",
        data: {
          authorities: [Authority.ADMIN]
        },
        canActivate: [UserRouteAccesService],
        
        loadChildren:()=> import('../components/detail-user/detail-user.module')
        .then(m => m.DetailUserModule)
      },
      {
        path:"program",
        data: {
          authorities: [Authority.ADMIN]
        },
        canActivate: [UserRouteAccesService],
        
        loadChildren:()=> import('../components/program/program.module')
        .then(m => m.ProgramModule)
      },
      {
        path:"ficha",
        data: {
          authorities: [Authority.ADMIN]
        },
        canActivate: [UserRouteAccesService],
        
        loadChildren:()=> import('../components/ficha/ficha.module')
        .then(m => m.FichaModule)
      },
  /*    {
        path: "list-file",
        component: FichaListComponent
      },*/
      /**ROUTES FOR ADMIN END**/
      /**ROUTES FOR INSTRUCTOR AND APRENDIZ**/
      {
        path:"call-attention",
        data: {
          authorities: [Authority.ADMIN, Authority.APRENDIZ, Authority.INSTRUCTOR]
        },
        canActivate: [UserRouteAccesService],
        
        loadChildren:()=> import('../components/call-attention/call-attention.module')
        .then(m => m.CallAttentionModule)
      },
      /**ROUTES FOR INSTRUCTOR AND APRENDIZ END**/
       /** Lazy Loading full End*/
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class dashboardRoutingModule { }
