import { NgModule } from '@angular/core';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatStepperModule } from '@angular/material/stepper';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatBadgeModule } from '@angular/material/badge';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDialogModule } from '@angular/material/dialog';
import { MatRippleModule } from '@angular/material/core';







@NgModule({
    imports: [
        MatInputModule,
        MatFormFieldModule,
        MatButtonModule,
        MatStepperModule,
        MatToolbarModule,
        MatBadgeModule,
        MatIconModule,
        MatSelectModule,
        MatDatepickerModule,
        MatMomentDateModule,
        MatCheckboxModule,
        MatExpansionModule,
        MatSidenavModule,
        MatButtonToggleModule,
        MatCardModule,
        MatSlideToggleModule,
        MatTooltipModule,
        MatDialogModule,
        MatRippleModule

    ],
    exports: [
        MatInputModule,
        MatFormFieldModule,
        MatButtonModule,
        MatStepperModule,
        MatToolbarModule,
        MatIconModule,
        MatSelectModule,
        MatBadgeModule,
        MatMomentDateModule,
        MatDatepickerModule,
        MatExpansionModule,
        MatSidenavModule,
        MatButtonToggleModule,
        MatCardModule,
        MatSlideToggleModule,
        MatTooltipModule,
        MatDialogModule,
        MatRippleModule
    ]
})
export class MaterialModule { }
